package com.javasampleapproach.rabbitmq.jsonconversion;

import java.io.File;
import java.util.ArrayList;
import java.io.*;
import java.util.List;

public class ReadFileToList {

    public ReadFileToList() {
    }

    public CustomMessage[] rd(BufferedReader abc, List<String> lines) throws IOException {
        String line;
        lines.clear();
        while ((line = abc.readLine()) != null){
            lines.add(line);
        }
        abc.close();

        CustomMessage[] act = new CustomMessage[lines.size()];
        for(int i = 0; i < lines.size(); i++) {
            act[i] = new CustomMessage(lines.get(i));
        }
        return act;
    }


}
