package com.javasampleapproach.rabbitmq.jsonconversion;

public class CustomMessage {

    private int patient_id;
    private String activity;
    private String start;
    private String end;


    public CustomMessage(int patient_id, String activity, String start, String end) {
        this.patient_id = patient_id;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }

    public int getPatient_id() {
        return patient_id;
    }

    public String getActivity() {
        return activity;
    }

    public String getStart() {
        return start;
    }

    public String getEnd() {
        return end;
    }

    public void setPatient_id(int patient_id) {
        this.patient_id = patient_id;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public void setEnd(String end) {
        this.end = end;
    }


    public CustomMessage(String line) {
        String[] split = line.split("\\t+");
        patient_id = 1;
        start = split[0];
        end = split[1];
        activity = split[2];
    }

    @Override
    public String toString() {
        return "{\n" +
                "\"patient_id=\": " + patient_id + ",\n" +
                "\"activity\": \"" + activity + "\",\n" +
                "\"start\": \"" + start + "\",\n" +
                "\"end\": \"" + end + "\"\n" +
                '}';
    }
}
