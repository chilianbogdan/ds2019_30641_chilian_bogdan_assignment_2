package com.javasampleapproach.rabbitmq;

import com.javasampleapproach.rabbitmq.producer.Producer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileNotFoundException;
import java.io.IOException;

@SpringBootApplication
public class SpringRabbitMqProducerApplication {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(SpringRabbitMqProducerApplication.class, args);
	}
}
