Pentru utilizarea aplicatie se va vor pune in functiune cele 2 proiecte:
-SpringRabbitMqProducer
-SpringRabbitMqConsumer

Pentru trimiterea mesajelor se vor accesa link-ul de mai jos:

http://localhost:8080/send?msg=Hello%20World!

Se poate observa in consola aplicatiei SpringRabbitMqProducer trimiterea mesajelor la secunda sub forma json.
In consola aplicatiei SpringRabbitMqConsumer se vor afisa doar activitatiile ce incalca regula.