package com.javasampleapproach.rabbitmq.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import com.google.gson.Gson;
@Component
public class Consumer {

    //CustomMessage[] act=new CustomMessage[1200];
    //public int ii=0;
    Work wk = new Work();


	@RabbitListener(queues="${jsa.rabbitmq.queue}")
    public void recievedMessage(String msg)
    {
	    //System.out.println("Recieved Message: " + msg);
	    wk.proces(msg);

    }
}
